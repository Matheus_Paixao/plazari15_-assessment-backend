<?php

namespace App\Controller;

use App\Repositories\ProductsRepository;

/**
 * Controller responsável pelo gerenciamento de categorias de produtos.
 *
 * Class CategoriesController
 * @package App\Controller
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class DashboardController extends BaseController
{
    /**
     * Responsável por exibir todos os itens das categorias;
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function index()
    {
        $Products = new ProductsRepository();
        $products = $Products->all();

        $this->setVariables('count', $products->count());
        $this->setVariables('arrProds', $products->toArray());
        echo $this->render('dashboard.php');
    }
}
