<?php

namespace App\Controller;

use App\Repositories\CategoriesRepository;
use App\Repositories\ProductsRepository;

/**
 * Controller responsável pelo gerenciamento de produtos.
 *
 * Class ProductsController
 * @package App\Controller
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class ProductsController extends BaseController
{
    /**
     * Responsável por exibir todos os itens dos produtos;
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function index()
    {
        $ProductsRepository = new ProductsRepository();

        $products = $ProductsRepository->all();

        $this->setVariables('message', flash()->display());
        $this->setVariables('products', $products);
        echo $this->render('products/products.php');
    }

    /**
     * Responsável por exibir a tela de criação dos produtos
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function create()
    {
        $CategoriesRepository = new CategoriesRepository();
        $categories = $CategoriesRepository->all();

        $this->setVariables('message', flash()->display());
        $this->setVariables('categories', $categories);
        $this->setVariables('formRoute', '/products/store');

        echo $this->render('products/addProduct.php');
    }

    /**
     * Responsável por gravar os produtos no banco de dados
     * @param id Quando preenchido,  busca o produto, caso exista editar, caso não, criar
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function store($id = null)
    {
        if($id) {
            $product_find = [
                'sku'   => $this->post('sku'),
                'id'    => $id
            ];
        } else {
            $product_find = [
                'sku'   => $this->post('sku'),
            ];
        }

       $product = [
           'sku' => $this->post('sku'),
           'name' => $this->post('name'),
           'price' => number_format($this->post('price'), 2, '.', ''),
           'description' => $this->post('description'),
           'quantity' => $this->post('quantity'),
           'categories' => $_POST['categories']
       ];

       $ProductsRepository = new ProductsRepository();
       $product_id = $ProductsRepository->save($product_find, $product);

        if ($product_id) {
            \flash()->success('Yeah! Your product has been created!');
            header('Location: /products');
        }
    }

    /**
     * Responsável por carregar e permitir a edição dos produtos;
     * @param id ID do produto no banco de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function edit($id)
    {
        $ProductsRepository = new ProductsRepository();
        $Product = $ProductsRepository->get($id);

        $CategoriesRepository = new CategoriesRepository();
        $categories = $CategoriesRepository->all();

        if (!$Product) {
            \flash()->error('Oops! Product Not Found!');
            header('Location: /products');
        }

        $this->setVariables('product', $Product->toArray());
        $this->setVariables('categories', $categories);
        $this->setVariables('formRoute', '/products/update/'.$id);
        echo $this->render('products/addProduct.php');
    }

    /**
     * Responsável por deletar o produto do banco e desfazer relacionamento;
     * @param id ID do produto no banco de dados
     * @author  Pedro Lazari <plazari96@gmail.com>
     */
    public function delete($id)
    {
        $ProductRepository = new ProductsRepository();

        $product = $ProductRepository->delete($id);

        if ($product) {
            \flash()->success('Yeah! Your product has been deleted!');
            return header('Location: /products');
        }

        \flash()->error('Oops! Your product has not been deleted!');
        return header('Location: /products');
    }
}
