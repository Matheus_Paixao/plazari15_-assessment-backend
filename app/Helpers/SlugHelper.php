<?php

namespace App\Helpers;

/**
 * Classe para métodos auxiliares de Slug
 * Class SlugHelper
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class SlugHelper
{

    /**
     * Função apenas para modificar uma string em slug.
     *
     * @param $string texto exscrito com espaços
     * @return array|string texto convertido em slug
     */
    public static function toSlug($string)
    {
        $slug = explode(' ', $string);
        $slug = implode('-', $slug);
        $slug = strtolower($slug);

        return $slug;
    }
}
