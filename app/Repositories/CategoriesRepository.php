<?php


namespace App\Repositories;


use App\Traits\LoggerTrait;
use App\Models\Category;

/**
 * Classe repositories de Categoria
 *
 * Class CategoriesRepository
 * @package App\Repositories
 */
class CategoriesRepository
{
    use LoggerTrait;

    /**
     * Retorna um Array com todas as categorias cadastradas no banco de Dados
     *
     * @return array Categorias
     */
    public function all()
    {
        $this->logInfo("Exibindo todas as categorias");
        return Category::all()->toArray();
    }

    /**
     * Busca uma categoria por Nome ou Código
     * @param string $category Nome ou Código da Categoria
     */
    public function findCategory(string $category)
    {
        $this->logInfo("Buscando uma categoria por Nome ou Código");
        $Category = Category::where(function ($query) use ($category) {
           $query->orWhere('name', $category);
           $query->orWhere('code', $category);
        });

        return $Category->first();
    }

    /**
     * Busca no banco uma categoria especifica e a retorna
     *
     * @param $id int categoria a ser buscada
     * @return Array com dados da categoria
     */
    public function getCategory($id)
    {
        $this->logInfo('Buscando categoria  id:'.$id);

        $Category = Category::find($id);

        return $Category->toArray();
    }


    /**
     * Busca pela categoria através do Array de validation e grava através
     * do Array de Category, caso vazio, array de category não será usado.
     *
     * @param array $validation dados a serem buscados no banco dados
     * @param array $category Dados a serem gravados no banco de dados
     * @return int ID da categoria criada ou atualizada
     */
    public function save(array $validation, array $category = [])
    {
        $this->logCritical("Iniciando Save da Categoria");
        $category = empty($category) ? $validation : $category;

        try {
            $this->log->info("Gravando categoria no Banco de Dados");
            $Category = Category::updateOrCreate($validation, $category);
        } catch (\Exception $e) {
            $this->logCritical("Erro ao gravar categoria no Banco de Dados", $e->getMessage());
            return false;
        }

        $this->logInfo("Categoria gravada no banco de dados", $Category->toArray());

        return $Category->id;
    }

    /**
     * Deleta a categoria do banco de dados.
     *
     * @param $id int da categoria a ser excluída
     * @return bool true se for removido.
     */
    public function delete($id)
    {
        $this->logInfo("Removendo a categoria {$id}");
        return Category::find($id)->delete();
    }

}
