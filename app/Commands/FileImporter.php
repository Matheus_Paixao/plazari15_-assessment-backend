<?php


namespace App\Commands;

require __DIR__ . '/../../bootstrap/cli.php';

use App\Helpers\SlugHelper;
use App\Repositories\CategoriesRepository;
use App\Repositories\ProductsRepository;
use App\Traits\LoggerTrait;
use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Exception;
use splitbrain\phpcli\Options;

/**
 * Class FileImporter
 * @package App\Commands
 */
class FileImporter extends CLI
{

    use LoggerTrait;

    private $filePath;
    private $delimiter = ';';
    private $enclosure = ' ';


    /**
     * Registra o Commando com as Opçoes que devem ser passadas
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function setup(Options $options)
    {
        $options->setHelp("Guia do Importador de Produtos e Categorias");
        $options->registerArgument('filePath', "Caminho para o arquivo. Coloque o caminho completo", 'f');
    }

    /**
     * A Programação do Comdando
     *
     * Argumentos e Opções vão aqui para serem recuperados
     *
     * @param Options $options
     * @return void
     *
     * @throws Exception
     */
    protected function main(Options $options)
    {
        $this->filePath = $options->getArgs()[0];
        if($this->filePath && file_exists($this->filePath)) {
            $this->info('Arquivo encontrado. Iniciando...');
            $this->processFile();
        }else{
            $this->error('Arquivo não encontrado. Saindo...');
        }


    }

    /**
     * Processamento do Arquivo é feito aqui
     */
    protected function processFile()
    {
        $this->logInfo("Iniciando importação de um arquivo CSV");
        $this->info("Abrindo arquivo...");
        $fileContent = fopen($this->filePath, 'r');
        if($fileContent) {
            $ProductRepository = new ProductsRepository();
            $fileHeader = fgetcsv($fileContent, 0, $this->delimiter, $this->enclosure);
            $total = $totalOk = $totalError = 0;
            while (!feof($fileContent)) {
                $row = fgetcsv($fileContent, 0, $this->delimiter, $this->enclosure);
                if (!$row) {
                    continue;
                }

                $line = array_combine($fileHeader, $row);
                $product = [
                    'sku'           => $line['sku'],
                    'name'          => $line['nome'],
                    'description'   => $line['descricao'],
                    'quantity'      => $line['quantidade'],
                    'price'         => number_format($line['preco'], 2, '.', ''),
                    'categories'    => $this->getCategories($line['categoria'])
                ];
                var_dump($product);
                $product_id = $ProductRepository->save([
                    'sku' => $product['sku']
                ], $product);

                if ($product_id) {
                    $totalOk++;
                } else {
                    $totalError++;
                }

                $total++;
            }

            $this->info("Foram encontrados {$total} produtos para serem registrados");
            $this->info("Foram registrados {$totalOk} produtos");

            if ($totalError > 0) {
                $this->warning("Falharam {$totalError} produtos");
            }

        }

    }

    /**
     * Busca no banco de dados a Categoria, se existir utiliza, se não, cria.
     *
     * @param string $categories Nome da Categoria
     * @return array de Categorias
     */
    private function getCategories(string $categories)
    {
        $categories_array = explode('|', $categories);
        $CategoryRepository = new CategoriesRepository();

        foreach ($categories_array as $key => $category) {
            $cat = $CategoryRepository->findCategory($category);

            if (!$cat) {
                $cat = $CategoryRepository->save([
                    'code' => SlugHelper::toSlug($category),
                    'name' => $category
                ]);
            } else {
                $cat = $cat->id;
            }

            $categories_array[$key] = $cat;
        }

        return $categories_array;
    }
}

// Inicia o Command
$cli = new FileImporter();
$cli->run();
