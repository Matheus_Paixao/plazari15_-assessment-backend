<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Classe model de produtos
 *
 * Class Product
 * @package App\Models
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class Product extends Model
{

    protected $fillable = [
        'sku',
        'name',
        'price',
        'quantity',
        'description'
    ];

    /**
     * Relacionamento N:N com a tabela de Categorias
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(){
        return $this->belongsToMany('App\Models\Category', 'products_categories');
    }
}
