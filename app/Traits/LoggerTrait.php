<?php

namespace App\Traits;

use Carbon\Carbon;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Classe responsável por chamar o Logger para registro de logs
 *
 * Class LoggerTrait
 * @package App\Logger
 * @author
 */
trait LoggerTrait
{
    public $log;

    private function boot()
    {
        $strem_hangler = $this->getStreamHandler();
        $Logger = new Logger('log');
        $this->log = $Logger->pushHandler($strem_hangler);
    }

    /**
     * Função responsável apenas por criar o nome e caminho do arquivo de Log
     *
     * @return StreamHandler
     * @throws \Exception
     */
    private function getStreamHandler()
    {
        $date = Carbon::now()->format('Y-m-d');
        return new StreamHandler(__DIR__ . '/../../logs/'.$date.'-logger.log', Logger::DEBUG);
    }


    /**
     * Cria Log do tipo INFO com nome do método e array de retorno
     * @param $text Mensagem que compoe o log
     * @param array $array Array com informações adicionais para serem gravadas
     */
    public function logInfo($text, $array = [])
    {
        $this->registerLog('info', __METHOD__ . ':: ' . $text, [$array]);
    }

    /**
     * Cria Log do tipo ERRO com nome do método e array de retorno
     * @param $text Mensagem que compoe o log
     * @param array $array Array com informações adicionais para serem gravadas
     */
    public function logError($text, $array = [])
    {
        $this->registerLog('error', __METHOD__ . ':: ' . $text, [$array]);
    }

    /**
     * Cria Log do tipo CRITICAL com nome do método e array de retorno
     * @param $text Mensagem que compoe o log
     * @param array $array Array com informações adicionais para serem gravadas
     */
    public function logCritical($text, $array = [])
    {
        $this->registerLog('critical', __METHOD__ . ':: ' . $text, [$array]);
    }

    /**
     * Cria Log do tipo ATENÇÃO com nome do método e array de retorno
     * @param $text Mensagem que compoe o log
     * @param array $array Array com informações adicionais para serem gravadas
     */
    public function logWarning($text, $array = [])
    {
        $this->registerLog('warning', __METHOD__ . ':: ' . $text, [$array]);
    }

    /**
     * Metodo para registrar o LOG no arquivo.
     * @param $text Mensagem que compoe o log
     * @param array $array Array com informações adicionais para serem gravadas
     */
    private function registerLog($method, $text, $array)
    {
        $this->boot();
        $this->log->$method($text, $array);
    }
}
