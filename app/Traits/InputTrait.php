<?php

namespace App\Traits;

/**
 * Classe contem métodos de tratamento de inputs
 *
 * Class InputTrait
 * @package App\Trait
 * @author Pedro Lazari <plazari96@gmail.com>
 */
trait InputTrait
{

    /**
     * Tratamento do Post, verificando se existem strings inválidas
     *
     * @param $key Recebido pelo formulário
     * @return mixed|string Retorna a string tratada
     */
    public function post($key)
    {
       $input = filter_input(INPUT_POST, $key);
       $input = strip_tags($input);
       $input = html_entity_decode($input, ENT_COMPAT, 'UTF-8');
       $input = htmlspecialchars($input, ENT_QUOTES, 'UTF-8');

       return $input;
    }

    /**
     * Tratamento do Método GET, recebe os parametros
     * @param $key Recebido por query string
     * @return mixed|string Retorna tratada a string
     */
    public function get($key)
    {
        $input = filter_input(INPUT_GET, $key);
        $input = strip_tags($input);
        $input = html_entity_decode($input, ENT_COMPAT, 'UTF-8');
        $input = htmlspecialchars($input, ENT_QUOTES, 'UTF-8');

        return $input;
    }

}
