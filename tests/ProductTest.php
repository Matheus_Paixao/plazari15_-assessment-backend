<?php

use App\Helpers\SlugHelper;
use App\Models\Category;
use App\Models\Product;
use PHPUnit\Framework\TestCase;

require __DIR__ . '/../bootstrap/cli.php';

/**
 * Class ProductTest
 */
class ProductTest extends TestCase
{
    /**
     * Teste Cria o produto e valida a existencia do SKU no retorno da Model
     */
    public function testCreateCategory()
    {
        $product = new Product();
        $prod = $product->create([
            'sku'           => '5fdsfsd-d',
            'name'          => 'PHP UNIT',
            'description'   => 'MUSSUM IPSUN',
            'quantity'      => 10,
            'price'         => number_format('12000', 2, '.', ''),
            'categories'    => []
        ]);

        $this->assertEquals('5fdsfsd-d', $prod->sku);
    }

    /**
     * Teste valida se a categoria foi excluída do banco de dadoss buscando pelo código
     */
    public function testDeleteCategory()
    {
        $product = new Product();
        $prod = $product->where('sku', '5fdsfsd-d')->delete();

        $this->assertEquals(true, $prod);
    }
}
