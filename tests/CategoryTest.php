<?php

use App\Helpers\SlugHelper;
use App\Models\Category;
use PHPUnit\Framework\TestCase;

require __DIR__ . '/../bootstrap/cli.php';

/**
 * Classe de Teste da Categoria, criando e deletando
 *
 * Class CategoriesTest
 */
class CategoryTest extends TestCase
{
    /**
     * Teste Cria a categoria e valida a existencia do código no retorno da Model
     */
    public function testCreateCategory()
    {
        $category = new Category();
        $cat = $category->create([
            'name'          => 'PHP Unit Teste',
            'code'          => SlugHelper::toSlug('php unit cat'),
        ]);

        $this->assertEquals('php-unit-cat', $cat->code);
    }

    /**
     * Teste valida se a categoria foi excluída do banco de dadoss buscando pelo código
     */
    public function testDeleteCategory()
    {
        $category = new Category();
        $cat = $category->where('code', 'php-unit-cat')->delete();

        $this->assertEquals(true, $cat);
    }
}
