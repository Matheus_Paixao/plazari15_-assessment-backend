<?php

require_once __DIR__ . '/../vendor/autoload.php';

Dotenv\Dotenv::create(__DIR__ . '/../')->load();

require_once __DIR__ . '/../database/connection/index.php';
