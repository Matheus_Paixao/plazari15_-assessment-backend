<?php

/**
 * Arquivo responsável por inciar o Framework
 * @author Pedro Lazari <plazari96@gmail.com>
 */
session_start();

require_once __DIR__ . '/../vendor/autoload.php';

Dotenv\Dotenv::create(__DIR__ . '/../')->load();

require_once __DIR__ . '/../database/connection/index.php';

require_once __DIR__ . '/../routes/routes.php';
