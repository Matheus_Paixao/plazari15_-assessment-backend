<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class Products
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class Products extends AbstractMigration
{
    /**
     * Metodo realiza Migration da tabela de Produtos
     */
    public function change()
    {
        $table = $this->table('products');
        $table->addColumn('sku', 'string')
            ->addColumn('name', 'string')
            ->addColumn('price', 'decimal', [
                'precision' => 10,
                'scale' => 2
            ])
            ->addColumn('description', 'text')
            ->addColumn('quantity', 'integer')
            ->addColumn('created_at', 'datetime',[
                'null' => true
            ])
            ->addColumn('updated_at', 'datetime', [
                'null' => true
            ])
            ->create();
    }

    public function up(){
        //
    }

    public function down(){
        //
    }
}
