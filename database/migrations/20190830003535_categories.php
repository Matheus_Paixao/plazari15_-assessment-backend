<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class Categories
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class Categories extends AbstractMigration
{
    /**
     * Metodo Realiza migration da tabela de Categorias
     */
    public function change()
    {
        $table = $this->table('categories');
        $table->addColumn('code', 'string')
            ->addColumn('name', 'string')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->create();

    }

    public function up(){
        //
    }

    public function down(){
        //
    }
}
