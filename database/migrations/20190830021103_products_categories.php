<?php

use Phinx\Migration\AbstractMigration;

/**
 * Class ProductsCategories
 *
 * @author Pedro Lazari <plazari96@gmail.com>
 */
class ProductsCategories extends AbstractMigration
{
    /**
     * Função realiza Migration ou Rollback do banco de dados de relacionamento
     * entre categoria e Produtos
     */
    public function change()
    {
        $table = $this->table('products_categories', ['id' => false]);

        $table = $table->addColumn('product_id', 'integer')
                    ->addForeignKey('product_id', 'products', 'id',
                        ['delete'=> 'CASCADE', 'update'=> 'CASCADE']);

        $table->addColumn('category_id', 'integer')
            ->addForeignKey('category_id', 'categories', 'id',
            ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
        ->create();

    }
}
