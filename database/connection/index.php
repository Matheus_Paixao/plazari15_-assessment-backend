<?php

/**
 * Arquivo de conexão com o banco de dados
 * @author Pedro Lazari <plazari96@gmail.com>
 */
use Illuminate\Database\Capsule\Manager as Capsule;


$databseConfig = require_once __DIR__ . '/../../app/config/database.php';


$illuminateConnection = new Capsule;
$illuminateConnection->addConnection($databseConfig);
$illuminateConnection->setAsGlobal();
$illuminateConnection->bootEloquent();

